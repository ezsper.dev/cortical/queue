import * as path from 'path';
import * as fs from 'fs';
import { GlobSync } from 'glob';
import { hooks } from '@cortical/core';
import { config, ConfigSource } from '@cortical/core/config';
import { getApiPath } from '@cortical/core/paths';
import { getWorkerQueues } from './Queue';
import { defaultValues } from '.';

hooks.bootstrap.addAction('queue', () => {
  const queueConfig = Object.assign({}, (<any>config).queue, defaultValues.queue);
  queueConfig.ui = Object.assign({}, queueConfig.ui, defaultValues.queue.ui);

  const serverLaunchInfoListener = hooks.serverLaunchInfo.addFilter('queue', (items) => {
    return [
      ...items,
      `🛎️ Queue management is available at http://${config.server.host}${queueConfig.ui.path}`,
    ];
  }, { priority: 20 });

  const middlewareListener = hooks.middlewares.addAction('queue-ui', ({ api }) => {
    const Arena = require('@ezsper/bull-arena');
    api.use((req, res, next) => {
      const queueMap: any = getWorkerQueues();
      const queues: any[] = [];

      for (const queueClient of queueMap) {
        const { queue } = queueClient;
        queues.push({
          name: queue.name,
          hostId: 'local',
          type: 'bull',
          prefix: queue.options.prefix,
        });
      }

      return Arena({
        queues,
      }, {
        basePath: queueConfig.ui.path,
        disableListen: true,
      })(req, res, next);
    });
  });

  if (queueConfig.disabled || queueConfig.ui.disabled) {
    serverLaunchInfoListener.disable();
    middlewareListener.disable();
  }

  if (!queueConfig.disabled) {
    const queuePath = path.resolve(getApiPath(), 'queues');
    if (fs.existsSync(queuePath)) {
      const glob = new GlobSync('*.@(ts|js)', {
        ignore: ['*.d.ts'],
        cwd: queuePath,
      });
      for (const file of glob.found) {
        const fileName = path.resolve(queuePath, file);
        require(fileName);
      }
    }
  }
});

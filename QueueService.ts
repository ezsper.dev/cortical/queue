import { JobOptions } from 'bull';
import { BaseContext } from '@cortical/core';
import { Job, Queue, QueueClient } from './Queue';

// TODO: Fixed client connect

export class QueueService<C extends BaseContext, Q extends Queue<any>> {

  constructor(protected context: C, protected queue: Q) {
    const exec = async (handler: any) => {
      if (typeof this.queueClient === 'undefined') {
        this.queueClient = this.queue.createClient();
      }
      try {
        return await handler();
      } catch (error) {
        if (error.message === 'Connection is closed.') {
          // TODO: Better handling to reconnect
          try {
            delete this.queueClient;
            await this.connect();
          } catch (error) {
            throw error;
          }
          return await handler();
        }
      }
    }
    this.get = async (...args: any[]) => {
      return await exec(async () => {
        return await (<any>this).queueClient.get(...args);
      });
    };
    this.dispatch = async (...args: any[]) => {
      return await exec(async () => {
        return await (<any>this).queueClient.dispatch(...args);
      });
    };
    this.context.destroy(() => {
      if (typeof this.queueClient !== 'undefined') {
        this.queueClient.close()
          .catch(error => console.log(error.stack));
      }
    });
  }

  protected queueClient = this.queue.createClient();

  async connect() {
    if (typeof this.queueClient === 'undefined') {
      this.queueClient = this.queue.createClient();
    }
    return this.queueClient;
  }

  async disconnect() {
    if (typeof this.queueClient !== 'undefined') {
      const { queueClient } = this;
      delete this.queueClient;
      await queueClient.close();
    }
  }

  get = this.queueClient.get;
  dispatch = this.queueClient.dispatch;

}

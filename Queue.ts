import * as Bull from 'bull';
import * as cluster from 'cluster';
import {
  BaseContext,
  ContextParams,
  createContext,
  hooks,
} from '@cortical/core';
import {
  logger,
  LogOptions,
  LogExec,
} from '@spine/logger';

export type ContextParamsHandler<A extends {}, T extends any = any, P extends any = any> = ContextParams | ((job: Job<A, T, P>) => ContextParams);

export interface QueueOptions extends Bull.QueueOptions {
  serializer?: {
    parse: (value: string) => any;
    stringify: (value: any) => string;
  };
}

export interface Job<A extends {}, T extends any = any, P extends any = any> {
  id: string | number;
  data: A;
  progress(value: P): Promise<void>;
  update(data: A): Promise<void>;
  getState(): Promise<Bull.JobStatus | 'stuck'>;
  remove(): Promise<void>;
  retry(): Promise<void>;
  finished(): Promise<T>;
  promote(): Promise<void>;
}

export type ProcessHandler<A extends {}, T extends any = any, C extends BaseContext = BaseContext, P extends any = any> = (job: Job<A, T, P>, context: C, client: QueueWorkerClient<A, T, C, P>) => Promise<T>;

export class QueueClient<A extends {}, T extends any = any, C extends BaseContext = BaseContext, P extends any = any> {

  protected bullQueue: Bull.Queue;

  constructor(protected queue: Queue<A, T, C, P>) {
    this.bullQueue = new Bull(queue.name, queue.options);
  }

  /**
   * Returns a promise that resolves when Redis is connected and the queue is ready to accept jobs.
   * This replaces the `ready` event emitted on Queue in previous verisons.
   */
  async isReady(): Promise<this> {
    await this.bullQueue.isReady();
    return this;
  }

  async get(id: string | number): Promise<Job<A, T, P>> {
    return (<any>this).bullQueue.getJob(id);
  }

  async dispatch(name: string | number, data: A, options?: Bull.JobOptions): Promise<Job<A, T, P>>;
  async dispatch(data: A, options?: Bull.JobOptions): Promise<Job<A, T, P>>;
  async dispatch(...args: any[]): Promise<Job<A, T, P>> {
    return await (<any>this).bullQueue.add(...args);
  }

  isWorker(): boolean {
    return false;
  }

  async close(): Promise<void> {
    try {
      return await (<any>this).bullQueue.close();
    } catch (error) {
      throw <Error>error;
    }
  }

}

export class QueueWorkerClient<A extends {}, T extends any = any, C extends BaseContext = BaseContext, P extends any = any> extends QueueClient<A, T, C, P> {

  constructor(protected queue: Queue<A, T, C, P>, concurrency: number = 1) {
    super(queue);
    if ((<any>queue).handler == null) {
      throw new Error(`A process handler must be specified`);
    }
    workerQueues.push(this);
    (<any>this).bullQueue.process(concurrency, async (job: any) => {
      let context: BaseContext | undefined;
      try {
        context = await queue.createContext(job);
        const result = await (<any>queue).handler(job, context, this);
        if (!context.completed) {
          context.end();
        }
        return result;
      } catch (error) {
        console.log(error.stack);
        setTimeout(() => {
          if (typeof context !== 'undefined' && !context.completed) {
            context.end(error);
          }
        }, 100);
        throw error;
      }
    });
  }

  isWorker(): boolean {
    return true;
  }

}

export class Queue<A extends {} = any, T extends any = any, C extends BaseContext = BaseContext, P extends any = any> {

  constructor(public name: string, public options?: QueueOptions) {}

  protected contextParamsHandler: ContextParamsHandler<A>;

  contextParams(handler: ContextParamsHandler<A>) {
    this.contextParamsHandler = handler;
  }

  getContextParams(job: Job<A, T, P>): ContextParams {
    const contextParams = this.contextParamsHandler;
    if (typeof contextParams === 'undefined') {
      return {};
    }
    if (typeof contextParams === 'function') {
      return contextParams(job);
    }
    return contextParams;
  }

  createContext(job: Job<A, T, P>): Promise<C> {
    return <any>createContext(this.getContextParams(job), this.name, 'queue', job.id);
  }

  protected handler: ProcessHandler<A, T, C, P>;

  process(handler: ProcessHandler<A, T, C, P>) {
    if (this.handler != null) {
      throw new Error(`A process handler was already specified`);
    }
    this.handler = handler;
  }

  async worker(concurrency: number = 1): Promise<QueueClient<A, T, C, P>> {
    return new QueueWorkerClient<A, T, C, P>(this, concurrency);
  }

  createClient() {
    return new QueueClient<A, T, C, P>(this);
  }

  log = logger(`queue:${this.name}`);

  async subscribe(): Promise<QueueClient<A, T, C, P>> {
    return this.createClient();
  }

  start(concurrency: number = 1, workers: number = 1) {
    if (workers > 0 && concurrency > 0) {
      switch (process.env.CORTICAL_COMMAND) {
        case 'start':
        case 'develop':
          hooks.init.addAction(`queue:${this.name}`, async () => {
            if (cluster.isMaster) {
              this.log.info(`Started with ${workers}x${concurrency}`);
            }
            if (cluster.isMaster && workers > 1) {
              for (let i = 0; i < workers; i++) {
                cluster.fork();
              }
            } else if (concurrency > 0) {
              await this.worker(concurrency)
                .catch(error => console.log(error.stack));
            }
          });
          break;
      }
    }
  }

}

const workerQueues: QueueWorkerClient<any, any>[] = [];

export function getWorkerQueues() {
  return [...workerQueues];
}

export * from './Queue';
export * from './QueueService';

export interface QueueConfig {
  queue: {
    disabled: boolean;
    ui: {
      disabled: boolean;
      path: string;
    };
  };
}

export const defaultValues: QueueConfig = {
  queue: {
    disabled: false,
    ui: {
      disabled: false,
      path: '/queues',
    },
  },
};
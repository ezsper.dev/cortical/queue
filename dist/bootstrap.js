"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fs = require("fs");
const glob_1 = require("glob");
const core_1 = require("@cortical/core");
const config_1 = require("@cortical/core/config");
const paths_1 = require("@cortical/core/paths");
const Queue_1 = require("./Queue");
const _1 = require(".");
core_1.hooks.bootstrap.addAction('queue', () => {
    const queueConfig = Object.assign({}, config_1.config.queue, _1.defaultValues.queue);
    queueConfig.ui = Object.assign({}, queueConfig.ui, _1.defaultValues.queue.ui);
    const serverLaunchInfoListener = core_1.hooks.serverLaunchInfo.addFilter('queue', (items) => {
        return [
            ...items,
            `🛎️ Queue management is available at http://${config_1.config.server.host}${queueConfig.ui.path}`,
        ];
    }, { priority: 20 });
    const middlewareListener = core_1.hooks.middlewares.addAction('queue-ui', ({ api }) => {
        const Arena = require('@ezsper/bull-arena');
        api.use((req, res, next) => {
            const queueMap = Queue_1.getWorkerQueues();
            const queues = [];
            for (const queueClient of queueMap) {
                const { queue } = queueClient;
                queues.push({
                    name: queue.name,
                    hostId: 'local',
                    type: 'bull',
                    prefix: queue.options.prefix,
                });
            }
            return Arena({
                queues,
            }, {
                basePath: queueConfig.ui.path,
                disableListen: true,
            })(req, res, next);
        });
    });
    if (queueConfig.disabled || queueConfig.ui.disabled) {
        serverLaunchInfoListener.disable();
        middlewareListener.disable();
    }
    if (!queueConfig.disabled) {
        const queuePath = path.resolve(paths_1.getApiPath(), 'queues');
        if (fs.existsSync(queuePath)) {
            const glob = new glob_1.GlobSync('*.@(ts|js)', {
                ignore: ['*.d.ts'],
                cwd: queuePath,
            });
            for (const file of glob.found) {
                const fileName = path.resolve(queuePath, file);
                require(fileName);
            }
        }
    }
});
//# sourceMappingURL=bootstrap.js.map
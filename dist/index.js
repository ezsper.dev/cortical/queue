"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./Queue"));
__export(require("./QueueService"));
exports.defaultValues = {
    queue: {
        disabled: false,
        ui: {
            disabled: false,
            path: '/queues',
        },
    },
};
//# sourceMappingURL=index.js.map
import * as Bull from 'bull';
import { BaseContext, ContextParams } from '@cortical/core';
export declare type ContextParamsHandler<A extends {}, T extends any = any, P extends any = any> = ContextParams | ((job: Job<A, T, P>) => ContextParams);
export interface QueueOptions extends Bull.QueueOptions {
    serializer?: {
        parse: (value: string) => any;
        stringify: (value: any) => string;
    };
}
export interface Job<A extends {}, T extends any = any, P extends any = any> {
    id: string | number;
    data: A;
    progress(value: P): Promise<void>;
    update(data: A): Promise<void>;
    getState(): Promise<Bull.JobStatus | 'stuck'>;
    remove(): Promise<void>;
    retry(): Promise<void>;
    finished(): Promise<T>;
    promote(): Promise<void>;
}
export declare type ProcessHandler<A extends {}, T extends any = any, C extends BaseContext = BaseContext, P extends any = any> = (job: Job<A, T, P>, context: C, client: QueueWorkerClient<A, T, C, P>) => Promise<T>;
export declare class QueueClient<A extends {}, T extends any = any, C extends BaseContext = BaseContext, P extends any = any> {
    protected queue: Queue<A, T, C, P>;
    protected bullQueue: Bull.Queue;
    constructor(queue: Queue<A, T, C, P>);
    /**
     * Returns a promise that resolves when Redis is connected and the queue is ready to accept jobs.
     * This replaces the `ready` event emitted on Queue in previous verisons.
     */
    isReady(): Promise<this>;
    get(id: string | number): Promise<Job<A, T, P>>;
    dispatch(name: string | number, data: A, options?: Bull.JobOptions): Promise<Job<A, T, P>>;
    dispatch(data: A, options?: Bull.JobOptions): Promise<Job<A, T, P>>;
    isWorker(): boolean;
    close(): Promise<void>;
}
export declare class QueueWorkerClient<A extends {}, T extends any = any, C extends BaseContext = BaseContext, P extends any = any> extends QueueClient<A, T, C, P> {
    protected queue: Queue<A, T, C, P>;
    constructor(queue: Queue<A, T, C, P>, concurrency?: number);
    isWorker(): boolean;
}
export declare class Queue<A extends {} = any, T extends any = any, C extends BaseContext = BaseContext, P extends any = any> {
    name: string;
    options?: QueueOptions | undefined;
    constructor(name: string, options?: QueueOptions | undefined);
    protected contextParamsHandler: ContextParamsHandler<A>;
    contextParams(handler: ContextParamsHandler<A>): void;
    getContextParams(job: Job<A, T, P>): ContextParams;
    createContext(job: Job<A, T, P>): Promise<C>;
    protected handler: ProcessHandler<A, T, C, P>;
    process(handler: ProcessHandler<A, T, C, P>): void;
    worker(concurrency?: number): Promise<QueueClient<A, T, C, P>>;
    createClient(): QueueClient<A, T, C, P>;
    log: import("@spine/logger").Log;
    subscribe(): Promise<QueueClient<A, T, C, P>>;
    start(concurrency?: number, workers?: number): void;
}
export declare function getWorkerQueues(): QueueWorkerClient<any, any, BaseContext, any>[];

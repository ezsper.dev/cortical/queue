"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
// TODO: Fixed client connect
class QueueService {
    constructor(context, queue) {
        this.context = context;
        this.queue = queue;
        this.queueClient = this.queue.createClient();
        this.get = this.queueClient.get;
        this.dispatch = this.queueClient.dispatch;
        const exec = (handler) => __awaiter(this, void 0, void 0, function* () {
            if (typeof this.queueClient === 'undefined') {
                this.queueClient = this.queue.createClient();
            }
            try {
                return yield handler();
            }
            catch (error) {
                if (error.message === 'Connection is closed.') {
                    // TODO: Better handling to reconnect
                    try {
                        delete this.queueClient;
                        yield this.connect();
                    }
                    catch (error) {
                        throw error;
                    }
                    return yield handler();
                }
            }
        });
        this.get = (...args) => __awaiter(this, void 0, void 0, function* () {
            return yield exec(() => __awaiter(this, void 0, void 0, function* () {
                return yield this.queueClient.get(...args);
            }));
        });
        this.dispatch = (...args) => __awaiter(this, void 0, void 0, function* () {
            return yield exec(() => __awaiter(this, void 0, void 0, function* () {
                return yield this.queueClient.dispatch(...args);
            }));
        });
        this.context.destroy(() => {
            if (typeof this.queueClient !== 'undefined') {
                this.queueClient.close()
                    .catch(error => console.log(error.stack));
            }
        });
    }
    connect() {
        return __awaiter(this, void 0, void 0, function* () {
            if (typeof this.queueClient === 'undefined') {
                this.queueClient = this.queue.createClient();
            }
            return this.queueClient;
        });
    }
    disconnect() {
        return __awaiter(this, void 0, void 0, function* () {
            if (typeof this.queueClient !== 'undefined') {
                const { queueClient } = this;
                delete this.queueClient;
                yield queueClient.close();
            }
        });
    }
}
exports.QueueService = QueueService;
//# sourceMappingURL=QueueService.js.map
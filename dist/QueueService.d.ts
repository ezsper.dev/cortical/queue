import { JobOptions } from 'bull';
import { BaseContext } from '@cortical/core';
import { Job, Queue, QueueClient } from './Queue';
export declare class QueueService<C extends BaseContext, Q extends Queue<any>> {
    protected context: C;
    protected queue: Q;
    constructor(context: C, queue: Q);
    protected queueClient: QueueClient<any, any, BaseContext, any>;
    connect(): Promise<QueueClient<any, any, BaseContext, any>>;
    disconnect(): Promise<void>;
    get: (id: string | number) => Promise<Job<any, any, any>>;
    dispatch: {
        (name: string | number, data: any, options?: JobOptions | undefined): Promise<Job<any, any, any>>;
        (data: any, options?: JobOptions | undefined): Promise<Job<any, any, any>>;
    };
}

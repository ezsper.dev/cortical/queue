export * from './Queue';
export * from './QueueService';
export interface QueueConfig {
    queue: {
        disabled: boolean;
        ui: {
            disabled: boolean;
            path: string;
        };
    };
}
export declare const defaultValues: QueueConfig;
